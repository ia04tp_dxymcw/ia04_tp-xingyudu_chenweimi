# IA04_TP XingyuDU_ChenweiMI

## Introduction

Selon l'instruction, nous avons fait un service de vote ("serveur REST") implémentant différentes méthodes de vote pour que les agents puissent voter sur ce serveur.

3 handles sont implémentés:

- **/new_ballot**
- **/vote**
- **/result**

4 méthodes de vote sont réalisées:

- majority
- borda
- approval
- condorcet

Dans ce fichier **README.md**, nous allons vous présenter plus de details et les réponses du serveur dans les cas différents sur photo.

---

## Préparation

### Go Install

Notre application est normalement installable via un **go install**
Vous pouvez utiliser directement la commande comme ci-dessous.

```go
go install -v gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi@latest
```

Ensuite, vous trouverez le fichier exécutable **ia04** dans dossier **bin** sous le chemin **$GOPATH**.

```shell
$GOPATH/bin/ia04_tp-xingyudu_chenweimi.exe
```

---

### Création de ballot: /new_ballot

Vous pouvez créer un ballot de vote en utilisant cette commande.
Il faut que vous saisissiez les informations dans les champs comme les formats ci-dessous.

- **rule** de type string (Vous pouvez entrer un des méthodes **"majority"**, **"borda"**,**"approval"**, **"condorcet"**)
- **ddl** de type string (Tue Nov 8 23:00:00 UTC 2022)
- **vid** de type [string,...] (["ag_id1", "ag_id2", "ag_id3"])
- **alts** de type int (3)

#### Create vote success 201

![success1](./img/create_vote_success_201.jpg)

#### Bad request 400 ＆ Not implemented 501

![error1](./img/error_time_400.jpg)

![error2](./img/not_implemented_501.jpg)

---

### Gestion des votants: /vote

Vous pouvez enregistrer la préférence de chaque votant dans le ballot en utilisant cette commande.
De même, les informations propres sont nécessaires.

- **aid** de type string
- **vid** de type string. (vids sont ce que vous avez déjà créés dans ballot.)
- **prefs** de type [int, ...]
- **opt** de type int

#### Vote success 200

![success2](./img/vote_success_200.jpg)

#### Vote repetition 403 ＆ Deadline passed 503

![error3](./img/vote_exist_403.jpg)

![error4](./img/ddl_passed_503.jpg)

---

### Résultat: /result

Vous pouvez obtenir le résultat(le gagnant et rank) avec cette commande.

- **bid** de type string (bid est décidé par votre choix du ballot)

#### Request success 201

![success3](./img/request_success_201.jpg)

#### Request too early 425 ＆ Request not found 404

![error3](./img/request_too_early_425.jpg)

![error4](./img/request_vote_not_found_404.jpg)

---

### Réponse

Comme prévu, vous pouvez obtenir votre chaque réponse dans le terminal.

![listen](./img/listen.jpg)
