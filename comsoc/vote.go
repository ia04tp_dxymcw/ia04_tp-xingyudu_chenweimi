package comsoc

import (
	"errors"
	"reflect"
	"sort"
)

type Alternative int
type Profile [][]Alternative // dim 2 array
type Count map[Alternative]int

type AlternativeArray []Alternative

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if v == alt {
			return i
		}

	}

	return -1 // si alt n'existe pas dans prefs
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return rank(alt1, prefs) < rank(alt2, prefs)
}

// renvoie les meilleures alternatives pour un décompte donné
func maxCount(count Count) (bestAlts []Alternative) {
	max_count := 0
	for alt := range count {
		if count[alt] > max_count {
			max_count = count[alt]
			bestAlts = []Alternative{alt}
		} else if count[alt] == max_count { // Two alternatives have the same count of vote
			bestAlts = append(bestAlts, alt)
		}
	}
	return bestAlts
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
func checkProfile(prefs Profile) error {
	var max_length int
	//if it’s vide
	if prefs != nil {
		max_length = len(prefs[0])
	} else {
		return errors.New("Profile vide")
	}
	for i := 1; i < len(prefs); i++ {
		if len(prefs[i]) != max_length {
			return errors.New("profile non complet")
		}
	}
	// If there’s any double alternative
	for i := 0; i < len(prefs); i++ {
		for j := 0; j < len(prefs[i])-1; j++ {
			for k := j + 1; k < len(prefs[i]); k++ {
				if prefs[i][j] == prefs[i][k] {
					return errors.New("duplicated alternative")
				}
			}
		}
	}
	return nil
}

// rewrite sort.Interface
func (alt AlternativeArray) Len() int {
	return len(alt)
}
func (alt AlternativeArray) Less(i, j int) bool {
	return alt[i] < alt[j]
}
func (alt AlternativeArray) Swap(i, j int) {
	alt[i], alt[j] = alt[j], alt[i]
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	altscopy := make(AlternativeArray, 0)
	altscopy = append(altscopy, alts[:]...)
	sort.Sort(altscopy)
	for i := 0; i < len(prefs); i++ {
		prefscopy := make(AlternativeArray, 0)
		prefscopy = append(prefscopy, prefs[i][:]...)
		sort.Sort(prefscopy)
		if !reflect.DeepEqual(prefscopy, altscopy) {
			return errors.New("chaque alternative doir apparait que une fois dans Profile")
		}
	}
	return nil
}

func MajoritySWF(p Profile) (count Count, err error) {

	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	count = make(Count)
	for i := 0; i < len(p); i++ {

		count[p[i][0]] = count[p[i][0]] + 1
	}

	return count, nil

}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	bestAlts = maxCount(count)
	if err != nil {
		return nil, err
	}

	return bestAlts, nil
}

func BordaSWF(p Profile) (count Count, err error) {
	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	count = make(Count)
	nb_alt := len(p[0]) // number of alternatives
	for i := 0; i < len(p); i++ {
		for j := 0; j < len(p[i]); j++ {
			count[p[i][j]] += nb_alt - rank(p[i][j], p[i]) // 3 alts, rank 1 -> score=2, rank 3 -> score=0
		}
	}
	return count, nil
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	bestAlts = maxCount(count)
	if err != nil {
		return nil, err
	}

	return bestAlts, nil
}

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	err = checkProfile(p)
	if err != nil {
		return nil, err
	}

	count = make(Count)
	for i := 0; i < len(p); i++ {
		for j := 0; j < thresholds[i]; j++ {
			count[p[i][j]] += 1
		}
	}
	return count, err
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	bestAlts = maxCount(count)
	if err != nil {
		return nil, err
	}

	return bestAlts, nil
}

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	//get all of the alternatives
	altscopy := make(AlternativeArray, 0)
	altscopy = append(altscopy, p[0][:]...)
	err = checkProfileAlternative(p, altscopy)
	//Compare
	count := make(Count)
	for i := 0; i < len(altscopy)-1; i++ {
		for j := i + 1; j < len(altscopy); j++ {
			var countAlt1, countAlt2 int
			for k := 0; k < len(p); k++ {
				if isPref(altscopy[i], altscopy[j], p[k]) {
					countAlt1 += 1
				} else {
					countAlt2 += 1
				}
			}
			if countAlt1 < countAlt2 {
				count[altscopy[j]] += 1
			} else {
				count[altscopy[i]] += 1
			}
		}
	}
	bestAlts = maxCount(count)
	if len(bestAlts) != 1 { // too many best alternatives => no winner
		bestAlts = nil
	}
	return bestAlts, err
}

func TieBreak(bestAlts []Alternative) (alt Alternative, err error) {
	if len(bestAlts) == 0 {
		err = errors.New("no alternative")
		return alt, err
	}
	alt = bestAlts[0]
	return alt, nil
}
