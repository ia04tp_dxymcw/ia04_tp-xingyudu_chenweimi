package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/agt"
	server "gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/agt/ballotagent"
	client "gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/agt/voteragent"
	"gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/comsoc"
)

func request_newBallot(rule string, ddl string, voter_ids []string, alts int) {
	req := agt.Request_newBallot{
		Rule:      rule,
		Deadline:  ddl,
		Voter_ids: voter_ids,
		Alts:      alts,
	}

	url := "http://localhost:8080/new_ballot"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		fmt.Println(resp.StatusCode, resp.Status)
		return
	}
}

func request_result(ballot_id string) {
	req := agt.Response_ballotId{
		Ballot_id: ballot_id,
	}
	url := "http://localhost:8080/result"
	data, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Println(resp.StatusCode, resp.Status)
		return
	}
}

func getRandomPrefs(alts []comsoc.Alternative) (Prefs []comsoc.Alternative) {
	Prefs = make([]comsoc.Alternative, len(alts))
	copy(Prefs, alts)
	rand.Seed(time.Now().UnixMicro())
	rand.Shuffle(len(Prefs), func(i, j int) { Prefs[i], Prefs[j] = Prefs[j], Prefs[i] })
	return
}

func main() {
	const n = 10
	const url1 = ":8080"
	const url2 = "http://localhost:8080"
	rules := []string{"majority", "borda", "approval", "condorcet"}
	rand.Seed(time.Now().UnixMicro())
	rule := rules[rand.Intn(4)]

	clAgts := make([]client.RestClientAgent, 0, n)
	servAgt := server.NewRestServerAgent(url1)

	log.Println("démarrage du serveur...")
	go servAgt.Start() // server start

	log.Println("démarrage des clients...")
	alts := []comsoc.Alternative{1, 2, 3, 4, 5}
	voter_ids := make([]string, 0)
	for i := 0; i < n; i++ {
		id := fmt.Sprintf("ag_id%02d", i)
		pref := getRandomPrefs(alts)
		option := []int{1}
		agt := client.NewRestClientAgent(id, url2, "vote1", pref, option)
		voter_ids = append(voter_ids, id)
		clAgts = append(clAgts, *agt)
	}
	request_newBallot(rule, "Mon Nov 10 22:00:00 UTC 2023", voter_ids, 5)

	for _, agt := range clAgts {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(agt client.RestClientAgent) {
			go agt.Start()
		}(agt)
	}
	time.Sleep(3 * time.Second)
	request_result("vote1")
}
