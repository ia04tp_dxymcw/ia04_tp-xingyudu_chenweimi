package ballotagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/agt"
	"gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/comsoc"
)

type RestServerAgent struct {
	sync.Mutex
	reqCount int
	addr     string
	ballots  map[string]*Ballot
}

type Ballot struct {
	rule      string
	deadline  time.Time
	voter_ids map[string]int
	alts      int
	profile   comsoc.Profile
	opt       []int // approval
}

var rules = []string{"majority", "borda", "approval", "condorcet"}

func NewRestServerAgent(addr string) *RestServerAgent {
	ballots := make(map[string]*Ballot)
	return &RestServerAgent{addr: addr, ballots: ballots}
}

func (rsa *RestServerAgent) hdlNewBallot(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête
	var req agt.Request_newBallot
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest) //400
		fmt.Fprint(w, err.Error())
		return
	}
	// traitement de la requête
	bid := "vote" + strconv.Itoa(rsa.reqCount)
	var ir int
	for ir = 0; ir < len(rules); ir++ {
		if rules[ir] == req.Rule {
			break
		}
	}
	if ir == len(rules) {
		w.WriteHeader(http.StatusNotImplemented) //501
		log.Print("Rule doesn't exist. e.g. ")
		log.Println(rules)
		return
	}
	t, t_err := time.ParseInLocation("Mon Jan 2 15:04:05 UTC 2006", req.Deadline, time.Local)
	if t_err != nil {
		w.WriteHeader(http.StatusBadRequest) //400
		log.Println("Structure of deadline is wrong. e.g. Mon Jan 2 15:04:05 UTC 2006")
		return
	}
	newP := make(comsoc.Profile, len(req.Voter_ids))
	for i := 0; i < len(req.Voter_ids); i++ {
		newP[i] = make([]comsoc.Alternative, req.Alts)
	}
	newV := make(map[string]int)
	for i := 0; i < len(req.Voter_ids); i++ {
		newV[req.Voter_ids[i]] = i //waiting for their preferences
	}
	opt := make([]int, 0)
	newbal := &Ballot{req.Rule, t, newV, req.Alts, newP, opt}
	rsa.ballots[bid] = newbal      //store new ballot in the map
	var resp agt.Response_ballotId //send response
	resp.Ballot_id = bid
	w.WriteHeader(http.StatusCreated) //201
	log.Println(resp.Ballot_id + " created")
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) hdlVote(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête
	var req agt.Request_vote
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest) //400
		fmt.Fprint(w, err.Error())
		return
	}
	// traitement de la requête
	if _, ok := rsa.ballots[req.Vote_id]; ok { //this ballot exists
		myBallots := rsa.ballots[req.Vote_id]
		//check deadline
		t_now, _ := time.ParseInLocation("Mon Jan 2 15:04:05 UTC 2006", time.Now().Format("Mon Jan 2 15:04:05 UTC 2006"), time.Local)
		if myBallots.deadline.Unix() < t_now.Unix() {
			w.WriteHeader(http.StatusServiceUnavailable) //503
			log.Println("unable to create this vote because the voting deadline has passed")
			return
		}
		if _, ok := myBallots.voter_ids[req.Agent_id]; ok { //this agent exists
			if myBallots.voter_ids[req.Agent_id] != -1 {
				if len(req.Prefs) != myBallots.alts {
					w.WriteHeader(http.StatusBadRequest) //400
					log.Println("length of Prefs is not correct")
					return
				}
				if len(req.Options) > myBallots.alts {
					w.WriteHeader(http.StatusBadRequest) //400
					log.Println("length of Opt is not correct")
					return
				}
				myBallots.profile[myBallots.voter_ids[req.Agent_id]] = req.Prefs
				myBallots.voter_ids[req.Agent_id] = -1 // -1 means this agent has voted
				myBallots.opt = req.Options
			} else {
				w.WriteHeader(http.StatusForbidden) //403
				log.Println("unable to create existing vote")
				return
			}
		} else {
			w.WriteHeader(http.StatusBadRequest) //400
			log.Println("this agent doesn't exist")
			return
		}
		/*
			// check profile
			var resp agt.Response_profile //send response
			resp.Profile = myBallots.profile
			w.WriteHeader(http.StatusCreated) //201
			log.Println(resp.Profile)
			serial, _ := json.Marshal(resp)
			w.Write(serial)*/
	} else {
		w.WriteHeader(http.StatusBadRequest) //400
		log.Println("this ballot doesn't exist")
		return
	}
	w.WriteHeader(http.StatusOK) //200
	log.Println("succeed in creating this vote of this agent")
}

func (rsa *RestServerAgent) hdlResult(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête
	var req agt.Response_ballotId
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest) //400
		fmt.Fprint(w, err.Error())
		return
	}
	// traitement de la requête
	if _, ok := rsa.ballots[req.Ballot_id]; ok { //this ballot exists
		myBallots := rsa.ballots[req.Ballot_id]
		for _, v := range myBallots.voter_ids {
			if v != -1 {
				w.WriteHeader(http.StatusTooEarly) //425
				log.Println("some users haven't voted yet")
				return
			}
		}
		rule := myBallots.rule
		//log.Println(myBallots.opt)
		var resp agt.Response_result //send response
		var count comsoc.Count
		var winner []comsoc.Alternative
		switch rule {
		case "majority":
			count, _ = comsoc.MajoritySWF(myBallots.profile)
			winner, _ = comsoc.MajoritySCF(myBallots.profile)
		case "borda":
			count, _ = comsoc.BordaSWF(myBallots.profile)
			winner, _ = comsoc.BordaSCF(myBallots.profile)
		case "approval":
			count, _ = comsoc.ApprovalSWF(myBallots.profile, myBallots.opt)
			winner, _ = comsoc.ApprovalSCF(myBallots.profile, myBallots.opt)
		case "condorcet":
			winner, _ = comsoc.CondorcetWinner(myBallots.profile)
		default:
			w.WriteHeader(http.StatusBadRequest) //400
			fmt.Fprint(w, err.Error())
			return
		}
		log.Println(count)
		w.WriteHeader(http.StatusCreated)
		resp.Ranking = make([]int, myBallots.alts)
		for k, v := range count { // convert form map to array
			resp.Ranking[k-1] = v
		}
		resp.Winner, _ = comsoc.TieBreak(winner)
		serial, _ := json.Marshal(resp)
		w.Write(serial)
	} else {
		w.WriteHeader(http.StatusNotFound) //404
		log.Println("couldn't find this ballot")
		return
	}

}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusNotImplemented) //501
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rsa.hdlNewBallot)
	mux.HandleFunc("/vote", rsa.hdlVote)
	mux.HandleFunc("/result", rsa.hdlResult)
	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
func StartBallotServer() {
	server := RestServerAgent{sync.Mutex{}, 0, ":8080", make(map[string]*Ballot)}
	server.Start()
}
