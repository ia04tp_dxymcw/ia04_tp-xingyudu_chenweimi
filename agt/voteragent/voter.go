package voteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/agt"
	"gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/comsoc"
)

type RestClientAgent struct {
	id        string
	url       string
	ballot_id string
	prefs     []comsoc.Alternative
	opt       []int
}

func NewRestClientAgent(id string, url string, ballot_id string, prefs []comsoc.Alternative, option []int) *RestClientAgent {
	return &RestClientAgent{id, url, ballot_id, prefs, option}
}

func (rca *RestClientAgent) doVote() (err error) {
	req := agt.Request_vote{
		Agent_id: rca.id,
		Vote_id:  rca.ballot_id,
		Prefs:    rca.prefs,
		Options:  rca.opt,
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return err
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	switch resp.StatusCode {
	case http.StatusOK: //200
		log.Println("vote pris en compte")
	case http.StatusBadRequest: //400
		log.Println("bad Request")
		err = fmt.Errorf("bad Request")
		return
	case http.StatusForbidden: //403
		log.Println("vote déjà effectué")
		err = fmt.Errorf("vote déjà effectué")
		return
	case http.StatusNotImplemented: //501
		log.Println("not implemented")
		err = fmt.Errorf("not implemented")
		return
	case http.StatusServiceUnavailable: //503
		log.Println("la deadline est dépassée")
		err = fmt.Errorf("la deadline est dépassée")
		return
	}
	return
}

func (rca *RestClientAgent) Start() {
	log.Printf("démarrage de %s", rca.id)
	err := rca.doVote()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		fmt.Printf("%s vote %+v\n", rca.id, rca.prefs)

	}
}
