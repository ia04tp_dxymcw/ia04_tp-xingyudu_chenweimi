package agt

import "gitlab.utc.fr/ia04tp_dxymcw/ia04_tp-xingyudu_chenweimi/comsoc"

type Request_newBallot struct {
	Rule      string   `json:"rule"` //"majority","borda", "approval", "stv", "kemeny"
	Deadline  string   `json:"ddl"`  //"Sun Nov 6 23:00:00 UTC 2022"
	Voter_ids []string `json:"vid"`  //["ag_id1", "ag_id2", "ag_id3"]
	Alts      int      `json:"alts"` //1
}

type Request_vote struct {
	Agent_id string               `json:"aid"`   //"ag_id1"
	Vote_id  string               `json:"vid"`   //"vote1"
	Prefs    []comsoc.Alternative `json:"prefs"` //[1, 2, 3]
	Options  []int                `json:"opt"`   //[3]
}

type Response_ballotId struct {
	Ballot_id string `json:"bid"` //"vote1"
}

type Response_profile struct {
	Profile comsoc.Profile `json:"prof"`
}

type Response_result struct {
	Winner  comsoc.Alternative `json:"winner"` // 3
	Ranking []int              `json:"rank"`   //[2, 1, 3]
}
